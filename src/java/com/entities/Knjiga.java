/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Shiki
 */
@Entity
@Table(name = "KNJIGA")
@NamedQueries({
    @NamedQuery(name = "Knjiga.findAll", query = "SELECT k FROM Knjiga k"),
    @NamedQuery(name = "Knjiga.findByGodinaizdanja", query = "SELECT k FROM Knjiga k WHERE k.godinaizdanja = :godinaizdanja"),
    @NamedQuery(name = "Knjiga.findByRbrizdanja", query = "SELECT k FROM Knjiga k WHERE k.rbrizdanja = :rbrizdanja"),
    @NamedQuery(name = "Knjiga.findById", query = "SELECT k FROM Knjiga k WHERE k.id = :id")})
public class Knjiga extends Publikacija implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 255)
    @Column(name = "GODINAIZDANJA")
    private String godinaizdanja;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RBRIZDANJA")
    private long rbrizdanja;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ID")
//    private Long id;
    @JoinTable(name = "KNJIGA_AUTOR", joinColumns = {
        @JoinColumn(name = "KNJIGAID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "AUTORID", referencedColumnName = "AUTORID")})
    @ManyToMany
    private List<Autor> autorList;
//    @JoinColumn(name = "ID", referencedColumnName = "ID", insertable = false, updatable = false)
//    @OneToOne(optional = false)
//    private Publikacija publikacija;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "knjiga", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Poglavlje> poglavljeList;

    public Knjiga() {
    }

//    public Knjiga(Long id) {
//        this.id = id;
//    }
//
//    public Knjiga(Long id, long rbrizdanja) {
//        this.id = id;
//        this.rbrizdanja = rbrizdanja;
//    }

    public String getGodinaizdanja() {
        return godinaizdanja;
    }

    public void setGodinaizdanja(String godinaizdanja) {
        this.godinaizdanja = godinaizdanja;
    }

    public long getRbrizdanja() {
        return rbrizdanja;
    }

    public void setRbrizdanja(long rbrizdanja) {
        this.rbrizdanja = rbrizdanja;
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public List<Autor> getAutorList() {
        return autorList;
    }

    public void setAutorList(List<Autor> autorList) {
        this.autorList = autorList;
    }

//    public Publikacija getPublikacija() {
//        return publikacija;
//    }
//
//    public void setPublikacija(Publikacija publikacija) {
//        this.publikacija = publikacija;
//    }

    public List<Poglavlje> getPoglavljeList() {
        return poglavljeList;
    }

    public void setPoglavljeList(List<Poglavlje> poglavljeList) {
        this.poglavljeList = poglavljeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Knjiga)) {
            return false;
        }
        Knjiga other = (Knjiga) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Knjiga[ id=" + getId() + " ]";
    }
    
}
