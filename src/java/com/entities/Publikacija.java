/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Shiki
 */
@Entity
@Table(name = "PUBLIKACIJA")
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "Publikacija.findAll", query = "SELECT p FROM Publikacija p"),
    @NamedQuery(name = "Publikacija.findById", query = "SELECT p FROM Publikacija p WHERE p.id = :id"),
    @NamedQuery(name = "Publikacija.findByBrojPrimeraka", query = "SELECT p FROM Publikacija p WHERE p.brojPrimeraka = :brojPrimeraka"),
    @NamedQuery(name = "Publikacija.findByNaziv", query = "SELECT p FROM Publikacija p WHERE p.naziv = :naziv")})
public class Publikacija implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "BROJ_PRIMERAKA")
    private Integer brojPrimeraka;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAZIV")
    private String naziv;
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "publikacija")
//    private Dnevnenovine dnevnenovine;
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "publikacija")
//    private Casopis casopis;
    @JoinColumn(name = "IZDAVACID", referencedColumnName = "ID")
    @ManyToOne
    private Izdavac izdavacid;
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "publikacija")
//    private Knjiga knjiga;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "publikacija")
    private List<Zaduzenje> zaduzenjeList;

    public Publikacija() {
    }

    public Publikacija(Long id) {
        this.id = id;
    }

    public Publikacija(Long id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBrojPrimeraka() {
        return brojPrimeraka;
    }

    public void setBrojPrimeraka(Integer brojPrimeraka) {
        this.brojPrimeraka = brojPrimeraka;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

//    public Dnevnenovine getDnevnenovine() {
//        return dnevnenovine;
//    }
//
//    public void setDnevnenovine(Dnevnenovine dnevnenovine) {
//        this.dnevnenovine = dnevnenovine;
//    }
//
//    public Casopis getCasopis() {
//        return casopis;
//    }
//
//    public void setCasopis(Casopis casopis) {
//        this.casopis = casopis;
//    }

    public Izdavac getIzdavacid() {
        return izdavacid;
    }

    public void setIzdavacid(Izdavac izdavacid) {
        this.izdavacid = izdavacid;
    }

//    public Knjiga getKnjiga() {
//        return knjiga;
//    }
//
//    public void setKnjiga(Knjiga knjiga) {
//        this.knjiga = knjiga;
//    }

    public List<Zaduzenje> getZaduzenjeList() {
        return zaduzenjeList;
    }

    public void setZaduzenjeList(List<Zaduzenje> zaduzenjeList) {
        this.zaduzenjeList = zaduzenjeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publikacija)) {
            return false;
        }
        Publikacija other = (Publikacija) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Publikacija[ id=" + id + " ]";
    }
    
}
