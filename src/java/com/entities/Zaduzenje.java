/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Shiki
 */
@Entity
@Table(name = "ZADUZENJE")
@NamedQueries({
    @NamedQuery(name = "Zaduzenje.findAll", query = "SELECT z FROM Zaduzenje z"),
    @NamedQuery(name = "Zaduzenje.findByDatumrazduzenja", query = "SELECT z FROM Zaduzenje z WHERE z.datumrazduzenja = :datumrazduzenja"),
    @NamedQuery(name = "Zaduzenje.findByDatumzaduzenja", query = "SELECT z FROM Zaduzenje z WHERE z.datumzaduzenja = :datumzaduzenja"),
    @NamedQuery(name = "Zaduzenje.findByPublikacijaId", query = "SELECT z FROM Zaduzenje z WHERE z.zaduzenjePK.publikacijaId = :publikacijaId"),
    @NamedQuery(name = "Zaduzenje.findByClanId", query = "SELECT z FROM Zaduzenje z WHERE z.zaduzenjePK.clanId = :clanId")})
public class Zaduzenje implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ZaduzenjePK zaduzenjePK;
    @Column(name = "DATUMRAZDUZENJA")
    @Temporal(TemporalType.DATE)
    private Date datumrazduzenja;
    @Column(name = "DATUMZADUZENJA")
    @Temporal(TemporalType.DATE)
    private Date datumzaduzenja;
    @JoinColumn(name = "CLAN_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Clan clan;
    @JoinColumn(name = "PUBLIKACIJA_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Publikacija publikacija;

    public Zaduzenje() {
    }

    public Zaduzenje(ZaduzenjePK zaduzenjePK) {
        this.zaduzenjePK = zaduzenjePK;
    }

    public Zaduzenje(long publikacijaId, long clanId) {
        this.zaduzenjePK = new ZaduzenjePK(publikacijaId, clanId);
    }

    public ZaduzenjePK getZaduzenjePK() {
        return zaduzenjePK;
    }

    public void setZaduzenjePK(ZaduzenjePK zaduzenjePK) {
        this.zaduzenjePK = zaduzenjePK;
    }

    public Date getDatumrazduzenja() {
        return datumrazduzenja;
    }

    public void setDatumrazduzenja(Date datumrazduzenja) {
        this.datumrazduzenja = datumrazduzenja;
    }

    public Date getDatumzaduzenja() {
        return datumzaduzenja;
    }

    public void setDatumzaduzenja(Date datumzaduzenja) {
        this.datumzaduzenja = datumzaduzenja;
    }

    public Clan getClan() {
        return clan;
    }

    public void setClan(Clan clan) {
        this.clan = clan;
    }

    public Publikacija getPublikacija() {
        return publikacija;
    }

    public void setPublikacija(Publikacija publikacija) {
        this.publikacija = publikacija;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (zaduzenjePK != null ? zaduzenjePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zaduzenje)) {
            return false;
        }
        Zaduzenje other = (Zaduzenje) object;
        if ((this.zaduzenjePK == null && other.zaduzenjePK != null) || (this.zaduzenjePK != null && !this.zaduzenjePK.equals(other.zaduzenjePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Zaduzenje[ zaduzenjePK=" + zaduzenjePK + " ]";
    }
    
}
