/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Shiki
 */
@Embeddable
public class ZaduzenjePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "PUBLIKACIJA_ID")
    private long publikacijaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLAN_ID")
    private long clanId;

    public ZaduzenjePK() {
    }

    public ZaduzenjePK(long publikacijaId, long clanId) {
        this.publikacijaId = publikacijaId;
        this.clanId = clanId;
    }

    public long getPublikacijaId() {
        return publikacijaId;
    }

    public void setPublikacijaId(long publikacijaId) {
        this.publikacijaId = publikacijaId;
    }

    public long getClanId() {
        return clanId;
    }

    public void setClanId(long clanId) {
        this.clanId = clanId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) publikacijaId;
        hash += (int) clanId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ZaduzenjePK)) {
            return false;
        }
        ZaduzenjePK other = (ZaduzenjePK) object;
        if (this.publikacijaId != other.publikacijaId) {
            return false;
        }
        if (this.clanId != other.clanId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.ZaduzenjePK[ publikacijaId=" + publikacijaId + ", clanId=" + clanId + " ]";
    }
    
}
