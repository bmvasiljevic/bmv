/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Shiki
 */
@Entity
@Table(name = "DNEVNENOVINE")
@NamedQueries({
    @NamedQuery(name = "Dnevnenovine.findAll", query = "SELECT d FROM Dnevnenovine d"),
    @NamedQuery(name = "Dnevnenovine.findByBrojizdanja", query = "SELECT d FROM Dnevnenovine d WHERE d.brojizdanja = :brojizdanja"),
    @NamedQuery(name = "Dnevnenovine.findByDatumizdanja", query = "SELECT d FROM Dnevnenovine d WHERE d.datumizdanja = :datumizdanja"),
    @NamedQuery(name = "Dnevnenovine.findByTiraz", query = "SELECT d FROM Dnevnenovine d WHERE d.tiraz = :tiraz"),
    @NamedQuery(name = "Dnevnenovine.findById", query = "SELECT d FROM Dnevnenovine d WHERE d.id = :id")})
public class Dnevnenovine extends Publikacija implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BROJIZDANJA")
    private long brojizdanja;
    @Column(name = "DATUMIZDANJA")
    @Temporal(TemporalType.DATE)
    private Date datumizdanja;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIRAZ")
    private long tiraz;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ID")
//    private Long id;
//    @JoinColumn(name = "ID", referencedColumnName = "ID", insertable = false, updatable = false)
//    @OneToOne(optional = false)
//    private Publikacija publikacija;

    public Dnevnenovine() {
    }
//
//    public Dnevnenovine(Long id) {
//        this.id = id;
//    }
//
//    public Dnevnenovine(Long id, long brojizdanja, long tiraz) {
//        this.id = id;
//        this.brojizdanja = brojizdanja;
//        this.tiraz = tiraz;
//    }

    public long getBrojizdanja() {
        return brojizdanja;
    }

    public void setBrojizdanja(long brojizdanja) {
        this.brojizdanja = brojizdanja;
    }

    public Date getDatumizdanja() {
        return datumizdanja;
    }

    public void setDatumizdanja(Date datumizdanja) {
        this.datumizdanja = datumizdanja;
    }

    public long getTiraz() {
        return tiraz;
    }

    public void setTiraz(long tiraz) {
        this.tiraz = tiraz;
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

//    public Publikacija getPublikacija() {
//        return publikacija;
//    }
//
//    public void setPublikacija(Publikacija publikacija) {
//        this.publikacija = publikacija;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dnevnenovine)) {
            return false;
        }
        Dnevnenovine other = (Dnevnenovine) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Dnevnenovine[ id=" + getId() + " ]";
    }
    
}
