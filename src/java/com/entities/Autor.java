/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Shiki
 */
@Entity
@Table(name = "AUTOR")
@NamedQueries({
    @NamedQuery(name = "Autor.findAll", query = "SELECT a FROM Autor a"),
    @NamedQuery(name = "Autor.findByAutorid", query = "SELECT a FROM Autor a WHERE a.autorid = :autorid"),
    @NamedQuery(name = "Autor.findByIme", query = "SELECT a FROM Autor a WHERE a.ime = :ime"),
    @NamedQuery(name = "Autor.findByPrezime", query = "SELECT a FROM Autor a WHERE a.prezime = :prezime")})
public class Autor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AUTORID")
    private Long autorid;
    @Size(max = 255)
    @Column(name = "IME")
    private String ime;
    @Size(max = 255)
    @Column(name = "PREZIME")
    private String prezime;
    @ManyToMany(mappedBy = "autorList")
    private List<Knjiga> knjigaList;

    public Autor() {
    }

    public Autor(Long autorid) {
        this.autorid = autorid;
    }

    public Long getAutorid() {
        return autorid;
    }

    public void setAutorid(Long autorid) {
        this.autorid = autorid;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public List<Knjiga> getKnjigaList() {
        return knjigaList;
    }

    public void setKnjigaList(List<Knjiga> knjigaList) {
        this.knjigaList = knjigaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autorid != null ? autorid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autor)) {
            return false;
        }
        Autor other = (Autor) object;
        if ((this.autorid == null && other.autorid != null) || (this.autorid != null && !this.autorid.equals(other.autorid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Autor[ autorid=" + autorid + " ]";
    }
    
}
