/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Shiki
 */
@Entity
@Table(name = "CASOPIS")
@NamedQueries({
    @NamedQuery(name = "Casopis.findAll", query = "SELECT c FROM Casopis c"),
    @NamedQuery(name = "Casopis.findByBroj", query = "SELECT c FROM Casopis c WHERE c.broj = :broj"),
    @NamedQuery(name = "Casopis.findByKrataksadrzaj", query = "SELECT c FROM Casopis c WHERE c.krataksadrzaj = :krataksadrzaj"),
    @NamedQuery(name = "Casopis.findByMesec", query = "SELECT c FROM Casopis c WHERE c.mesec = :mesec"),
    @NamedQuery(name = "Casopis.findById", query = "SELECT c FROM Casopis c WHERE c.id = :id")})
public class Casopis extends Publikacija implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BROJ")
    private int broj;
    @Size(max = 255)
    @Column(name = "KRATAKSADRZAJ")
    private String krataksadrzaj;
    @Size(max = 255)
    @Column(name = "MESEC")
    private String mesec;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ID")
//    private Long id;
//    @JoinColumn(name = "ID", referencedColumnName = "ID", insertable = false, updatable = false)
//    @OneToOne(optional = false)
//    private Publikacija publikacija;

    public Casopis() {
    }
//
//    public Casopis(Long id) {
//        this.id = id;
//    }
//
//    public Casopis(Long id, int broj) {
//        this.id = id;
//        this.broj = broj;
//    }

    public int getBroj() {
        return broj;
    }

    public void setBroj(int broj) {
        this.broj = broj;
    }

    public String getKrataksadrzaj() {
        return krataksadrzaj;
    }

    public void setKrataksadrzaj(String krataksadrzaj) {
        this.krataksadrzaj = krataksadrzaj;
    }

    public String getMesec() {
        return mesec;
    }

    public void setMesec(String mesec) {
        this.mesec = mesec;
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

//    public Publikacija getPublikacija() {
//        return publikacija;
//    }
//
//    public void setPublikacija(Publikacija publikacija) {
//        this.publikacija = publikacija;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Casopis)) {
            return false;
        }
        Casopis other = (Casopis) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Casopis[ id=" + getId() + " ]";
    }
    
}
