/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Shiki
 */
@Embeddable
public class PoglavljePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "RBR")
    private int rbr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "KNJIGAID")
    private long knjigaid;

    public PoglavljePK() {
    }

    public PoglavljePK(int rbr, long knjigaid) {
        this.rbr = rbr;
        this.knjigaid = knjigaid;
    }

    public int getRbr() {
        return rbr;
    }

    public void setRbr(int rbr) {
        this.rbr = rbr;
    }

    public long getKnjigaid() {
        return knjigaid;
    }

    public void setKnjigaid(long knjigaid) {
        this.knjigaid = knjigaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) rbr;
        hash += (int) knjigaid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PoglavljePK)) {
            return false;
        }
        PoglavljePK other = (PoglavljePK) object;
        if (this.rbr != other.rbr) {
            return false;
        }
        if (this.knjigaid != other.knjigaid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.PoglavljePK[ rbr=" + rbr + ", knjigaid=" + knjigaid + " ]";
    }
    
}
