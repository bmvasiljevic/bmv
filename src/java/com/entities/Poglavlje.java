/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Shiki
 */
@Entity
@Table(name = "POGLAVLJE")
@NamedQueries({
    @NamedQuery(name = "Poglavlje.findAll", query = "SELECT p FROM Poglavlje p"),
    @NamedQuery(name = "Poglavlje.findByRbr", query = "SELECT p FROM Poglavlje p WHERE p.poglavljePK.rbr = :rbr"),
    @NamedQuery(name = "Poglavlje.findByNaziv", query = "SELECT p FROM Poglavlje p WHERE p.naziv = :naziv"),
    @NamedQuery(name = "Poglavlje.findByKnjigaid", query = "SELECT p FROM Poglavlje p WHERE p.poglavljePK.knjigaid = :knjigaid")})
public class Poglavlje implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PoglavljePK poglavljePK;
    @Size(max = 255)
    @Column(name = "NAZIV")
    private String naziv;
    @JoinColumn(name = "KNJIGAID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Knjiga knjiga;

    public Poglavlje() {
    }

    public Poglavlje(PoglavljePK poglavljePK) {
        this.poglavljePK = poglavljePK;
    }

    public Poglavlje(int rbr, long knjigaid) {
        this.poglavljePK = new PoglavljePK(rbr, knjigaid);
    }

    public PoglavljePK getPoglavljePK() {
        return poglavljePK;
    }

    public void setPoglavljePK(PoglavljePK poglavljePK) {
        this.poglavljePK = poglavljePK;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Knjiga getKnjiga() {
        return knjiga;
    }

    public void setKnjiga(Knjiga knjiga) {
        this.knjiga = knjiga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poglavljePK != null ? poglavljePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Poglavlje)) {
            return false;
        }
        Poglavlje other = (Poglavlje) object;
        if ((this.poglavljePK == null && other.poglavljePK != null) || (this.poglavljePK != null && !this.poglavljePK.equals(other.poglavljePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Poglavlje[ poglavljePK=" + poglavljePK + " ]";
    }
    
}
