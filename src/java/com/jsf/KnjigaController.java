package com.jsf;

import com.entities.Izdavac;
import com.entities.Knjiga;
import com.entities.Poglavlje;
import com.entities.PoglavljePK;
import com.jsf.util.JsfUtil;
import com.jsf.util.JsfUtil.PersistAction;
import com.session.KnjigaFacade;
import com.session.PoglavljeFacade;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.hibernate.Session;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

@Named("knjigaController")
@SessionScoped
public class KnjigaController implements Serializable {

    @EJB
    private com.session.KnjigaFacade ejbFacade;
    private List<Knjiga> items = null;
    private Knjiga selected;
    private Poglavlje selectedPoglavlje;
    private List<Knjiga> itemsIzdavac = null;

    public List<Knjiga> getItemsIzdavac() {
        return itemsIzdavac;
    }

    public void setItemsIzdavac(List<Knjiga> itemsIzdavac) {
        this.itemsIzdavac = itemsIzdavac;
    }

    public KnjigaController() {
    }
    
    public void ucitajKnjigeIzdavaca(Izdavac i){
        itemsIzdavac = ejbFacade.ucitajKnjigeIzdavaca(i);
    }
    
    public void refresh(){
        items = null;
    }
  
    public void postaviIzabranogIzdavaca(Izdavac i){
        selected.setIzdavacid(i);
    }

    public Knjiga getSelected() {
        return selected;
    }

    public void setSelected(Knjiga selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void setEmbeddableKeysPoglavlje() {
    }

    protected void initializeEmbeddableKey() {
        selected.setId(getMaxId());
    }

    protected void initializeEmbeddableKeyPoglavlje() {
        PoglavljePK ppk = new PoglavljePK();
        ppk.setKnjigaid(selected.getId());
        ppk.setRbr((int) getMaxIdPoglavlje());
        selectedPoglavlje.setPoglavljePK(ppk);
    }

    public long getMaxId() {
        long max = 0;
        for (Iterator<Knjiga> it = items.iterator(); it.hasNext();) {
            Knjiga knjiga = it.next();
            long value = knjiga.getId();
            if (value > max) {
                max = value;
            }
        }
        return max + 1;
    }

    public long getMaxIdPoglavlje() {
        long max = 0;
        for (Iterator<Poglavlje> it = selected.getPoglavljeList().iterator(); it.hasNext();) {
            Poglavlje poglavlje = it.next();
            long value = poglavlje.getPoglavljePK().getRbr();
            if (value > max) {
                max = value;
            }
        }
        return max + 1;
    }

    private KnjigaFacade getFacade() {
        return ejbFacade;
    }

    public Knjiga prepareCreate() {
        selected = new Knjiga();
        initializeEmbeddableKey();
        return selected;
    }

    public Poglavlje prepareCreatePoglavlje() {
        selectedPoglavlje = new Poglavlje();
        initializeEmbeddableKeyPoglavlje();
        return selectedPoglavlje;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("KnjigaCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void createPoglavlje() {
        persistPoglavlje(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PoglavljeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("KnjigaUpdated"));
    }

    public void updatePoglavlje() {
        persistPoglavlje(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PoglavljeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("KnjigaDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void destroyPoglavlje() {
        persistPoglavlje(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PoglavljeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selectedPoglavlje = null; // Remove selection
            items = null; // Invalidate list of items to trigger re-query
        }

    }

    public List<Knjiga> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void persistPoglavlje(PersistAction persistAction, String successMessage) {
        if (selectedPoglavlje != null) {
            setEmbeddableKeysPoglavlje();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().editPoglavlje(selected, selectedPoglavlje);
                } else {
                    getFacade().removePoglavlje(selected, selectedPoglavlje);
                }
                JsfUtil.addSuccessMessage(successMessage);

            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Knjiga getKnjiga(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<Knjiga> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Knjiga> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    public Poglavlje getSelectedPoglavlje() {
        return selectedPoglavlje;
    }

    public void setSelectedPoglavlje(Poglavlje selectedPoglavlje) {
        this.selectedPoglavlje = selectedPoglavlje;
    }

    @FacesConverter(forClass = Knjiga.class)
    public static class KnjigaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            KnjigaController controller = (KnjigaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "knjigaController");
            return controller.getKnjiga(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Knjiga) {
                Knjiga o = (Knjiga) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Knjiga.class.getName()});
                return null;
            }
        }

    }

}
