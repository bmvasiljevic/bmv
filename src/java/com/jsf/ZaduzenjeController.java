package com.jsf;

import com.entities.Zaduzenje;
import com.jsf.util.JsfUtil;
import com.jsf.util.JsfUtil.PersistAction;
import com.session.ZaduzenjeFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("zaduzenjeController")
@SessionScoped
public class ZaduzenjeController implements Serializable {

    @EJB
    private com.session.ZaduzenjeFacade ejbFacade;
    private List<Zaduzenje> items = null;
    private Zaduzenje selected;

    public ZaduzenjeController() {
    }

    public Zaduzenje getSelected() {
        return selected;
    }

    public void setSelected(Zaduzenje selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getZaduzenjePK().setClanId(selected.getClan().getId());
        selected.getZaduzenjePK().setPublikacijaId(selected.getPublikacija().getId());
    }

    protected void initializeEmbeddableKey() {
        selected.setZaduzenjePK(new com.entities.ZaduzenjePK());
    }

    private ZaduzenjeFacade getFacade() {
        return ejbFacade;
    }

    public Zaduzenje prepareCreate() {
        selected = new Zaduzenje();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ZaduzenjeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ZaduzenjeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ZaduzenjeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Zaduzenje> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Zaduzenje getZaduzenje(com.entities.ZaduzenjePK id) {
        return getFacade().find(id);
    }

    public List<Zaduzenje> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Zaduzenje> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Zaduzenje.class)
    public static class ZaduzenjeControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ZaduzenjeController controller = (ZaduzenjeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "zaduzenjeController");
            return controller.getZaduzenje(getKey(value));
        }

        com.entities.ZaduzenjePK getKey(String value) {
            com.entities.ZaduzenjePK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new com.entities.ZaduzenjePK();
            key.setPublikacijaId(Long.parseLong(values[0]));
            key.setClanId(Long.parseLong(values[1]));
            return key;
        }

        String getStringKey(com.entities.ZaduzenjePK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getPublikacijaId());
            sb.append(SEPARATOR);
            sb.append(value.getClanId());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Zaduzenje) {
                Zaduzenje o = (Zaduzenje) object;
                return getStringKey(o.getZaduzenjePK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Zaduzenje.class.getName()});
                return null;
            }
        }

    }

}
