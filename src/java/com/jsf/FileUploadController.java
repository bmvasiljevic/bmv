/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsf;

import com.jsf.util.JsfUtil;
import com.session.FileUploadFаcade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Shiki
 */
@Named(value = "fileUploadController")
@SessionScoped
public class FileUploadController implements Serializable {

    @EJB
    private FileUploadFаcade fileUploadFаcade;

    public FileUploadController() {
    }

    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void upload() {
        if (file != null) {

            boolean rez = fileUploadFаcade.uploadujFajl(file);

            if (rez) {
                JsfUtil.addSuccessMessage("Fajl je uspešno uploаd-ovan!!!");
            }else{
                JsfUtil.addErrorMessage("Fajl NIJE uspešno uploadovan!!!");
            }

        }
    }

}
