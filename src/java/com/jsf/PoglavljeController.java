package com.jsf;

import com.entities.Poglavlje;
import com.jsf.util.JsfUtil;
import com.jsf.util.JsfUtil.PersistAction;
import com.session.PoglavljeFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("poglavljeController")
@SessionScoped
public class PoglavljeController implements Serializable {

    @EJB
    private com.session.PoglavljeFacade ejbFacade;
    private List<Poglavlje> items = null;
    private Poglavlje selected;

    public PoglavljeController() {
    }

    public Poglavlje getSelected() {
        return selected;
    }

    public void setSelected(Poglavlje selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getPoglavljePK().setKnjigaid(selected.getKnjiga().getId());
    }

    protected void initializeEmbeddableKey() {
        selected.setPoglavljePK(new com.entities.PoglavljePK());
    }

    private PoglavljeFacade getFacade() {
        return ejbFacade;
    }

    public Poglavlje prepareCreate() {
        selected = new Poglavlje();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PoglavljeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PoglavljeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PoglavljeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Poglavlje> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Poglavlje getPoglavlje(com.entities.PoglavljePK id) {
        return getFacade().find(id);
    }

    public List<Poglavlje> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Poglavlje> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Poglavlje.class)
    public static class PoglavljeControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PoglavljeController controller = (PoglavljeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "poglavljeController");
            return controller.getPoglavlje(getKey(value));
        }

        com.entities.PoglavljePK getKey(String value) {
            com.entities.PoglavljePK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new com.entities.PoglavljePK();
            key.setRbr(Integer.parseInt(values[0]));
            key.setKnjigaid(Long.parseLong(values[1]));
            return key;
        }

        String getStringKey(com.entities.PoglavljePK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getRbr());
            sb.append(SEPARATOR);
            sb.append(value.getKnjigaid());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Poglavlje) {
                Poglavlje o = (Poglavlje) object;
                return getStringKey(o.getPoglavljePK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Poglavlje.class.getName()});
                return null;
            }
        }

    }

}
