/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.session;

import com.entities.Izdavac;
import com.entities.Knjiga;
import com.entities.Poglavlje;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import org.hibernate.Session;


/**
 *
 * @author Shiki
 */
@Stateless
public class KnjigaFacade extends AbstractFacade<Knjiga> {
    @PersistenceContext(unitName = "PrimefacesProbaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KnjigaFacade() {
        super(Knjiga.class);
    }
    
    public boolean contains(Knjiga k){
        return getEntityManager().contains(k);
    }

    public void editPoglavlje(Knjiga selected, Poglavlje selectedPoglavlje) {
//      Ukoliko kolekcija ne sadrzi poglavlje, dodaj ga (novo poglavlje).
//      U svakom slucaju uradi update knjige (knjiga ima CASCADE na poglavlja)
        
        boolean postoji = false;
//      provera se vrsi po kljucu poglavlja
        for (Poglavlje next : selected.getPoglavljeList()) {
            if(next.getPoglavljePK().equals(selectedPoglavlje.getPoglavljePK())){
                postoji = true;
            }
        }
        
        if(!postoji){
            selected.getPoglavljeList().add(selectedPoglavlje);
        }
        selected = em.merge(selected);
    }

    public void removePoglavlje(Knjiga selected, Poglavlje selectedPoglavlje) {
//      Napomena: ovaj pristup "radi" samo ukoliko je kolekcija 
//      anotirana sa orphanRemoval = true  
        selected.getPoglavljeList().remove(selectedPoglavlje);
        em.merge(selected);
    }

//    public void refresh(Knjiga selected) {
////        selected = em.find(Knjiga.class, selected.getId());  
//    }

    public List<Knjiga> ucitajKnjigeIzdavaca(Izdavac i) {
        List<Knjiga> filtriraneKnjige = new ArrayList<>();
        Session session = em.unwrap(Session.class);
        filtriraneKnjige = session.createQuery("from Knjiga k where k.izdavacid.id = " 
                                                + i.getId()).list();
        return filtriraneKnjige;
    }

    
}
