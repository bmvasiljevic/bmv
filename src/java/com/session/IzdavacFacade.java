/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.session;

import com.entities.Izdavac;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shiki
 */
@Stateless
public class IzdavacFacade extends AbstractFacade<Izdavac> {
    @PersistenceContext(unitName = "PrimefacesProbaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public IzdavacFacade() {
        super(Izdavac.class);
    }
    
}
