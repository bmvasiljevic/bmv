/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.session;

import com.entities.Dnevnenovine;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shiki
 */
@Stateless
public class DnevnenovineFacade extends AbstractFacade<Dnevnenovine> {
    @PersistenceContext(unitName = "PrimefacesProbaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DnevnenovineFacade() {
        super(Dnevnenovine.class);
    }
    
}
