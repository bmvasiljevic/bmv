/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.session;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Shiki
 */
@Stateless
public class FileUploadFаcade {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public boolean uploadujFajl(UploadedFile file) {

        try {
            FileOutputStream fos = new FileOutputStream("C:\\Users\\Shiki\\Desktop\\proba2\\" + file.getFileName());
            InputStream is = file.getInputstream();
            byte[] bajtovi = new byte[1024];
            int read = 0;

            while ((read = is.read(bajtovi)) != -1) {
                fos.write(bajtovi, 0, read);
            }
            fos.close();

            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileUploadFаcade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileUploadFаcade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
