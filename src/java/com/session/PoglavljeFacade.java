/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.session;

import com.entities.Poglavlje;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shiki
 */
@Stateless
public class PoglavljeFacade extends AbstractFacade<Poglavlje> {
    @PersistenceContext(unitName = "PrimefacesProbaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PoglavljeFacade() {
        super(Poglavlje.class);
    }
    
    public boolean contains(Poglavlje p ){
        return getEntityManager().contains(p);
    }

    @Override
    public void remove(Poglavlje entity) {
        entity = getEntityManager().merge(entity);
        entity.getKnjiga().getPoglavljeList().remove(entity);
        entity.setKnjiga(null);
        getEntityManager().remove(entity);
    }
    
    
    
}
